README This README would normally document whatever steps are necessary to get your application up and running.

What is this repository for? This access control system software codes is a keyless entry system that gives you total control over your commercial or 
residential premises by granting physical access to authorized users only. It is an essential part of any security solution. You can customize this 
system to fit your specific requirements and easily integrate it with other systems such as visitorsí management systems or alarm systems. 
(https://https://bitbucket.org/aynurg/accesscontrol) How do I get set up? Install Microsoft SQL 2015, Visual Studio 2019 Use SQL script file for
create database and tables Download all codes and AccessControl.sln Run software and The entry door opens automatically when the computer screen 
detects a face. License This project is licensed under the MIT License - see the below for details

Copyright (c) <2021> <copyright Burhan Kureshi>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.